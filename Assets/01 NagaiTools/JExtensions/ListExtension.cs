﻿﻿﻿using System.Collections.Generic;

namespace _01_UnityTools.JExtensions
{
    public static class ListExtension
    {
        public static void SetRange<T>(this IList<T> list, IEnumerable<int> range, T value)
        {
            foreach (var pos in range)
                list[pos] = value;
        }
    }
}