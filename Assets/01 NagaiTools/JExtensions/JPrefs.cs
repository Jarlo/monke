﻿﻿﻿using UnityEngine;

namespace _00_Game.Scripts
{
    public static class JPrefs
    {
        public static void SaveAs<T>(string key, T obj)
        {
            PlayerPrefs.SetString(key, JsonUtility.ToJson(obj));
        }
        
        public static T LoadAs<T>(string key)
        {
            return JsonUtility.FromJson<T>(PlayerPrefs.GetString(key));
        }

        public static bool TryLoadAs<T>(string key, out T obj)
        {
            if (PlayerPrefs.HasKey(key))
            {
                obj = LoadAs<T>(key);
                return true;
            }
            obj = default(T);
            return false;
        }
    }
}