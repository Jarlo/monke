using UnityEngine;

namespace _00_Game.Scripts.Tools
{
    public static class AnimationCurveExtensions
    {
        public static float CalcArea(this AnimationCurve curve, float step = 0.001f)
        {
            var time = curve[0].time;
            var end = curve[curve.length-1].time;
            var area = 0f;
            while (time < end)
            {
                area += curve.Evaluate(time + step/2) * step;
                time += step;
            }
            return area;
        }

        public static float CalcTimeLength(this AnimationCurve curve)
        {
            return curve[curve.length - 1].time - curve[0].time;
        }

        public static int EvalInt(this AnimationCurve curve, int x)
        {
            return Mathf.RoundToInt(curve.Evaluate(x));
        }
    }
}