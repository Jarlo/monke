﻿﻿﻿using System.Collections.Generic;
using System.Linq;

namespace _01_UnityTools.JExtensions
{
    public interface IRanged<T> : IList<T>{}
    
    public static class JRange
    {
        public static IEnumerable<int> CycledRange<T>(this IRanged<T> list, int start, int count) 
            => ModMap(Range(start, count), list.Count);
        
        public static IEnumerable<int> CycledRange(int start, int count, int divider) 
            => ModMap(Range(start, count), divider);

        public static IEnumerable<int> Range(int start, int count) 
            => Enumerable.Range(start, count);

        private static IEnumerable<int> ModMap(IEnumerable<int> enumerable, int divider)
            => enumerable.Select(i => i * divider);

        
        
        // public static void SetCycledRange<T>(this IRanged<T> list, IEnumerable<int> positions, T value)
        // {
        //     list.SetRange(ModMap(positions, list.Count), value);
        // }
        
        
    }
}