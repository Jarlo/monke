﻿﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _00_Game.Scripts
{
    public static class UnityExtensions
    {

        /// <summary>
        /// Extension method to check if a layer is in a layermask
        /// </summary>
        /// <param name="mask"></param>
        /// <param name="layer"></param>
        /// <returns></returns>
        public static bool Contains(this LayerMask mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }
        
        
        public static IEnumerable<Transform> GetChildren(this Transform parent)
        {
            for (var i = 0; i < parent.childCount; i++)
                yield return parent.GetChild(i);
        }
        
        
        public static void DestroyChildren(this Transform transform, Action<GameObject> clearDelegate)
        {
            transform
                .GetChildren()
                .Select(t => t.gameObject)
                .ToList()
                .ForEach(clearDelegate);
        }

        public static void DestroyChildren(this Transform transform)
        {
            if (Application.isPlaying)
                transform.DestroyChildren(GameObject.Destroy);
            else transform.DestroyChildren(GameObject.DestroyImmediate);
        }
    }
}