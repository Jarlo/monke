﻿﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _00_Game.Scripts.Tools.Extensions
{
    public static class GameObjectExtensions
    {
        public static void DestroyAllChildren(this GameObject go)
        {
            var children = go.GetComponentsInChildren<Transform>();
            foreach (var child in children)
            {
                if (child == go.transform)
                {
                    continue;
                }

                child.SetParent(null);
                Object.Destroy(child.gameObject);
            }
        }

        public static IEnumerable<Transform> GetChildren(this Transform _parent, bool terminal=true)
        {
            var stream = Enumerable
                .Range(0, _parent.childCount)
                .Select(_parent.GetChild);
            return terminal ? stream.ToArray() : stream;
        }
        
    }
}