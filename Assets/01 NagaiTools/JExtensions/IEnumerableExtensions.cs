﻿﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _01_UnityTools.JExtensions
{
    public static class IEnumerableExtensions
    {
        public static T[] Sorted<T>(this IEnumerable<T> enumerable) 
            where T: IComparable<T>
        {
            var array = enumerable.ToArray();
            Array.Sort(array);
            return array;
        }
        
        public static T[] Sorted<T>(this IEnumerable<T> enumerable, IComparer<T> comparer) 
            where T: IComparable<T>
        {
            var array = enumerable.ToArray();
            Array.Sort(array, comparer);
            return array;
        }

        public static T MaxBy<T>(this IList<T> array, Func<T, float> appraiser)
        {
            return MinBy(array, (item) => -appraiser(item));
        }
        
        public static T MinBy<T>(this IList<T> array, Func<T, float> appraiser)
        {
            var minEl = array.First();
            var minVal = appraiser(minEl);
            foreach (var element in array.Skip(1))
            {
                var val = appraiser(element);
                if (val < minVal)
                {
                    minEl = element;
                    minVal = val;
                }
            }
            return minEl;
        }
    }
}