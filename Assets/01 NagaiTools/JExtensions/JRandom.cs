using System;
using System.Collections.Generic;
using System.Linq;
using _01_UnityTools.JExtensions;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _00_Game.Scripts.Tools
{
    public static class JRandom
    {
        public static T Pick<T>(this IEnumerable<T> enumerable)
        {
            var iList = enumerable as IList<T> ?? enumerable.ToArray();
            if (iList.Any())
                return iList.ElementAt(iList.PickIndex());
            else return default;
        }

        public static T Pick<T>(this IEnumerable<T> enumerable, int seed)
        {
            SetSeed(seed);
            return enumerable.Pick();
        }

        public static void SetSeed(int seed)
        {
            Random.InitState(seed);
        }
        
        public static T PickOrDefault<T>(this IEnumerable<T> enumerable)
        {
            var list = enumerable.ToArray();
            if (list.Any())
                return list.ElementAt(list.PickIndex());
            else return default;
        }


        public static int Weighted(IList<float> weights)
        {
            var coeff = 1/weights.Sum();
            var normRand = Random.value;
            var prob = 0f;
            for (int i = 0; i < weights.Count; i++)
            {
                prob += weights[i] * coeff;
                if (normRand <= prob)
                    return i;
            }
            return weights.Count-1;
        }
        
        public static T Weighted<T>(params (T item, float weight)[] weightedItems)
        {
            var weights = weightedItems.Select(item => item.weight).ToArray();
            var index = Weighted(weights);
            return weightedItems[index].item;
        }

        public static T Weighted<T>(this IEnumerable<T> enumerable, Func<T, float> appraiser)
        {
            var items = enumerable.ToArray();
            var weights = items.Select(appraiser).ToArray();
            return items[Weighted(weights)];
        }

        public static int PickIndex<T>(this IList<T> list)
        {
            return Random.Range(0, list.Count());
        }
        
        public static IEnumerable<T> PickRange<T>(this IList<T> list, int length)
        {
            var start = list.PickIndex();
            return JRange.CycledRange(start, length, list.Count)
                .Select(index => list[index]);
        }
        

        public static bool Probability(float probability)
        {
            return Random.value <= probability;
        }

        public static void DoAfter(float delay, Action callback)
        {
            DOTween.Sequence().InsertCallback(delay, () => callback());
        }

        public static void DoAfterRand(float minDelay, float maxDelay, Action callback)
        {
            DoAfter(Random.Range(minDelay, maxDelay), callback);
        } 
        
        public static bool CoinFlip() => Probability(0.5f);

        public static IEnumerable<T> Sample<T>(this IList<T> source, int count)
        {
            for (int i = 0; i < count; i++)
                yield return source.Pick();
        }
        
        public static T[] ShuffleTo<T>(this IEnumerable<T> enumerable)
        {
            var array = enumerable.ToArray();
            Shuffle(list:array);
            return array;
        }

        public static void Shuffle<T>(this IList<T> list)  
        {  
            int n = list.Count;  
            while (n > 1) {  
                n--;  
                int k = Random.Range(0, n + 1);  
                T value = list[k];  
                list[k] = list[n];  
                list[n] = value;  
            }  
        }
    }
}