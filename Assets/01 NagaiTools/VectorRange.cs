﻿using System;
using UnityEngine;

namespace _00_Game.Scripts.Movement
{
    [Serializable]
    public class VectorRange
    {
        [SerializeField] private FloatRange xRange;
        [SerializeField] private FloatRange yRange;
        [SerializeField] private FloatRange zRange;
        
        public Vector3 Clamp(Vector3 val)
        {
            return new Vector3(xRange.Clamp(val.x), yRange.Clamp(val.y), zRange.Clamp(val.z));
        }
    }
}