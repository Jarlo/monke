﻿﻿using UnityEngine;

public class DestroyObjectTimer : MonoBehaviour
{
    [SerializeField] private float destroyTime = 1f;

    private void Start()
    {
        Destroy(gameObject, destroyTime);
    }
}
