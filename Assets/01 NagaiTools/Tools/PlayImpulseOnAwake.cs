﻿﻿using Cinemachine;
using UnityEngine;

namespace UnityTools.FeedbackManager
{
    public class PlayImpulseOnAwake : MonoBehaviour
    {
        private void Awake()
        {
            GetComponent<CinemachineImpulseSource>().GenerateImpulse();
        }
    }
}
