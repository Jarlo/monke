﻿using System;
using UnityEngine;

namespace _00_Game.Scripts.Movement
{
    [Serializable]
    public class FloatRange
    {
        public float min = -1;
        public float max = 1;
        public float Clamp(float val) => Mathf.Clamp(val, min, max);
        public float Lerp(float time) => Mathf.Lerp(min, max, time);
    }
}