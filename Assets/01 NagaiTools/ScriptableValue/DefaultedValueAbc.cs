﻿﻿namespace _01_NagaiTools
{
    public abstract class DefaultedValueAbc<T> : ScriptableValue<T>
    {
        public abstract T DefaultValue { get; }
        public void Reset() => SetValue(DefaultValue);
    }
}