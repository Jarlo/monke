﻿﻿using UnityEngine;

namespace _01_NagaiTools
{
     
    public class SharedValue<T> : DefaultedValueAbc<T>
    {
        [SerializeField] private T defaultValue;
        public override T DefaultValue => defaultValue;
        protected RuntimeContainer containedValue;
        protected RuntimeContainer ContainedValue
        {
            get
            {
                return containedValue ?? (containedValue = new RuntimeContainer(DefaultValue));
            }
        }

        protected override T GetValue() => ContainedValue.contained;
        protected override void SetValue(T val) => ContainedValue.contained = val;

        protected class RuntimeContainer
        {
            public T contained;
            public RuntimeContainer(T contained)
            {
                this.contained = contained;
            }
        }
    }
}