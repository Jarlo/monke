﻿﻿using _01_NagaiTools;
using UnityEngine;

public abstract class ProgressThroughSO : MonoBehaviour
{
    [SerializeField] protected SharedFloat progress;
    protected virtual void Start() => progress.OnValueChanged += OnChange;
    protected virtual void OnDestroy() => progress.OnValueChanged -= OnChange;
    protected void OnChange() => OnChange(progress.Value);

    protected abstract void OnChange(float newValue);
}