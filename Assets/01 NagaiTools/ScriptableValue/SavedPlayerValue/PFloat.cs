﻿﻿using UnityEngine;

namespace _01_NagaiTools
{
    public class PFloat : PFloatAbc
    {
        [SerializeField] protected float defaultValue;
        public override float DefaultValue => defaultValue;
    }
}