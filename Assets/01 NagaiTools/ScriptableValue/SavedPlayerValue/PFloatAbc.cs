﻿﻿using UnityEngine;

namespace _01_NagaiTools
{
    public abstract class PFloatAbc : DefaultedValueAbc<float>
    {
        [SerializeField] protected string prefsKey;
        
        protected override void SetValue(float val)
        {
            PlayerPrefs.SetFloat(prefsKey, val);
        }

        protected override float GetValue()
        {
            return PlayerPrefs.GetFloat(prefsKey, DefaultValue);
        }
    }
}