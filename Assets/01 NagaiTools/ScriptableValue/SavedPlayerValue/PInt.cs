﻿﻿using UnityEngine;

namespace _01_NagaiTools
{
    public class PInt : PIntAbc
    {
        [SerializeField] protected int defaultValue;
        public override int DefaultValue => defaultValue;
    }
}