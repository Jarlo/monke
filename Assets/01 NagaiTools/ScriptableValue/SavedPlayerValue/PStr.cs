﻿﻿using UnityEngine;

namespace _01_NagaiTools
{
    public class PStr : PStrAbc
    {
        [SerializeField] protected string defaultValue;
        public override string DefaultValue => defaultValue;
    }
}