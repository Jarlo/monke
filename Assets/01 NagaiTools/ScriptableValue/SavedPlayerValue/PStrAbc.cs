﻿﻿using UnityEngine;

namespace _01_NagaiTools
{
    public abstract class PStrAbc : DefaultedValueAbc<string>
    {
        [SerializeField] protected string prefsKey;
        
        protected override void SetValue(string val)
        {
            PlayerPrefs.SetString(prefsKey, val);
        }

        protected override string GetValue()
        {
            return PlayerPrefs.GetString(prefsKey, DefaultValue);
        }
    }
}