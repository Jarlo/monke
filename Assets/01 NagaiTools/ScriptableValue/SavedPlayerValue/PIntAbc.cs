﻿﻿using UnityEngine;

namespace _01_NagaiTools
{
    public abstract class PIntAbc : DefaultedValueAbc<int>
    {
        [SerializeField] protected string prefsKey;
        
        protected override void SetValue(int value)
        {
            PlayerPrefs.SetInt(prefsKey, value);
        }

        protected override int GetValue()
        {
            return PlayerPrefs.GetInt(prefsKey, DefaultValue);
        }
    }
}