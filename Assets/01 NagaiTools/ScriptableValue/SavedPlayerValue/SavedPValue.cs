﻿﻿using System;
using UnityEngine;

namespace _01_NagaiTools
{
    public abstract class SavedPValue<T> : ScriptableValue<T>
    {
        [SerializeField] private string prefsKey;
        public abstract T DefaultValue { get; }
        
        protected override void SetValue(T value)
        {
            PlayerPrefs.SetString(prefsKey, Serialize(value));
        }

        protected override T GetValue()
        {
            if (PlayerPrefs.HasKey(prefsKey))
                return Deserialize(PlayerPrefs.GetString(prefsKey));
            else
                return DefaultValue;
        }

        public abstract string Serialize(T val);
        public abstract T Deserialize(string val);
    }
}