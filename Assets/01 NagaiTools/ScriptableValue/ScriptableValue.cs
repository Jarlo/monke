﻿﻿using System;
using UnityEngine;

namespace _01_NagaiTools
{
    public abstract class ScriptableValue<T> : ScriptableObject
    {
        public event Action OnValueChanged;
        public T Value
        {
            get => GetValue();
            set
            {
                if (Equals(value, Value))
                    return;
                SetValue(value);
                OnValueChanged?.Invoke();
            }
        }

        protected abstract void SetValue(T value);
        protected abstract T GetValue();
    }
}