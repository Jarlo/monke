﻿﻿using UnityEngine;

namespace _00_Game.Scripts
{
    public abstract class ConfigSingletone<SelfT> : ScriptableObject 
        where SelfT : ConfigSingletone<SelfT>
    {
        private static SelfT instance;
        public static SelfT Instance
        {
            get
            {
                if (instance == null)
                {
                    var self = ScriptableObject.CreateInstance<SelfT>();
                    instance = self.StaticGet();
                }
                return instance;
            }
            private set => instance = value;
        }

        protected abstract SelfT StaticGet();
    }
}