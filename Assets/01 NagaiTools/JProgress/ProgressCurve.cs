﻿using System;
using UnityEngine;

namespace _00_Game.Scripts
{
    [Serializable]
    public class ProgressCurve
    {
        [SerializeField] public float min = 0;
        [SerializeField] public float max = 1;
        [SerializeField] private AnimationCurve curve;

        public float Evaluate(float normTime)
        {
            return min + curve.Evaluate(normTime) * (max - min);
        }
    }
}