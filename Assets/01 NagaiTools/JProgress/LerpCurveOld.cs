﻿using System;
using UnityEngine;

namespace _00_Game.Scripts
{

    // [Serializable]
    // public abstract class LerpCurve<T> : IDisposable
    // {
    //     public AnimationCurve progressMap; // [0,1] -> [0,1]
    //     public float duration;
    //     
    //     public T startValue;
    //     public T endValue;
    //
    //     public float StartTime { get; protected set; } = -1;
    //     public bool IsStarted { get; protected set; }
    //     public float EndTime => StartTime + duration;
    //     public bool IsFinished => Time.time >= EndTime;
    //
    //     public T Value => IsStarted ? Evaluate(Progress) : startValue;
    //     
    //     protected T Evaluate(float normTime)
    //     {
    //         return Lerp(progressMap.Evaluate(normTime));
    //     }
    //
    //     public void Start()
    //     {
    //         StartTime = Time.time;
    //         IsStarted = true;
    //     }
    //
    //     public void Dispose()
    //     {
    //         StartTime = -1;
    //         IsStarted = false;
    //     }
    //
    //     protected abstract T Lerp(float normTime);
    //
    //     public float Progress => ProgressMathf.CalcProgress(StartTime, duration, Time.time);
    // }
}