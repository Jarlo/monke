﻿﻿﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _00_Game.Scripts
{
    [Serializable]
    public class VectorWindow
    {
        [SerializeField] private float maxLen;
        private LinkedList<Vector3> list = new LinkedList<Vector3>();
        public int Count => list.Count;
        public Vector3 Sum { get; private set; }

        public void Add(Vector3 v)
        {
            if(list.Any())
            {
                var delta = v - list.Last.Value;
                Sum += delta;
            }
            list.AddLast(v);
            while (IsOverLong())
            {
                var first = list.First.Value;
                var second = list.First.Next.Value;
                var delta = second - first;
                Sum -= delta;
                list.RemoveFirst();
            }
        }

        private bool IsOverLong()
        {
            if (Count < 2)
                return false;
            return Sum.magnitude > maxLen;  //CalcSum().magnitude > maxLen;
        }

        public void DrawGizmos()
        {
            Gizmos.color = Color.red;
            var ps = list.ToArray();
            for (int i = 1; i < Count; i++)
                Gizmos.DrawLine(ps[i - 1], ps[i]);
        }
    }
}