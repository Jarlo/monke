﻿using System;
using UnityEngine;

namespace _00_Game.Scripts
{
    [Serializable]
    public class CurvedValue
    {
        [SerializeField] private TimeInterval interval;
        [SerializeField] private ProgressCurve curve;
        public bool IsStarted => interval.IsStarted;
        public bool IsEnded => interval.IsFinished;
        public void Start() => interval.Start();
        public void Reset() => interval = TimeInterval.Unarmed(interval.Duration);
        public float Value => IsStarted ? curve.Evaluate(interval.Progress) : curve.min;
    }
}