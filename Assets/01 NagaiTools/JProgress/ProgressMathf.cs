﻿using UnityEngine;

namespace _00_Game.Scripts
{
    public static class ProgressMathf
    {
        public static float CalcProgress(float start, float duration, float current)
        {
            return Mathf.Clamp01((current - start) / duration);
        }
    }
}