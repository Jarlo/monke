﻿using System;
using UnityEngine;

namespace _00_Game.Scripts
{
    [Serializable]
    public struct TimeInterval : ITimer
    {
        public float StartTime { get; private set; }
        public float Duration { get; private set; }
        public bool IsStarted { get; private set; }

        private TimeInterval(float duration)
        {
            Duration = duration;
            IsStarted = false;
            StartTime = -1;
        }
        
        public static TimeInterval Unarmed(float duration)
        {
            return new TimeInterval(duration);
        }
        
        public static TimeInterval FromNow(float duration)
        {
            var i = Unarmed(duration);
            i.Start();
            return i;
        }
        
        public float EndTime => StartTime + Duration;
        public bool IsFinished => Time.time >= EndTime;
        public float Progress => ProgressMathf.CalcProgress(StartTime, Duration, Time.time);
        
        public void Start()
        {
            IsStarted = true;
            StartTime = Time.time;
        }
    }
}