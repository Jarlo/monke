﻿using System;
using UnityEngine;

namespace _00_Game.Scripts
{
    [Serializable]
    public class ChangedCurvedValue
    {
        [SerializeField] private float baseDuration;
        [SerializeField] private TimeInterval interval;
        [SerializeField] private ProgressCurve curve;
        private TimeInterval realInterval;
        public bool IsStarted { get; private set; }

        public bool IsEnded => interval.IsFinished;
        
        public void Start(float durationCoefficient)
        {
            interval = TimeInterval.FromNow(baseDuration * durationCoefficient);
            IsStarted = true;
        }

        public void Reset()
        {
            IsStarted = false;
        }

        public float Value => IsStarted ? curve.Evaluate(interval.Progress) : curve.min;
    }
}