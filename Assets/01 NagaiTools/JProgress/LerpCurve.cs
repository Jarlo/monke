﻿using System;
using UnityEngine;

namespace _00_Game.Scripts
{
    public interface ILerper<T>
    {
        public T StartVal { get; }
        public T EndVal { get; }
        public T Lerp(float time);
    }

    public abstract class Lerper<T> : ILerper<T>
    {
        public T StartVal { get; protected set; }
        public T EndVal { get; protected set; }
        public Lerper(T startVal, T endValVal)
        {
            StartVal = startVal;
            EndVal = endValVal;
        }

        public T Lerp(float time) => Lerp(StartVal, EndVal, time);
        public abstract T Lerp(T start, T end, float time);
    }

    public class LerperVector3 : Lerper<Vector3>
    {
        public LerperVector3(Vector3 startVal, Vector3 endValVal) : base(startVal, endValVal) { }
        
        public override Vector3 Lerp(Vector3 start, Vector3 end, float time) 
            => Vector3.Lerp(start, end, time);
    }
    
    public interface IProgressable
    {
        public float Progress { get; }
    }
    
    public interface IProgressValue<T> : IProgressable
    {
        public T Value { get; }
    }
    

    public interface IStartable
    {
        public bool IsStarted { get; }
        public bool IsFinished { get; }
        public void Start();
    }

    public interface ITimer : IProgressable, IStartable { }

    public struct LerpCurve<T> : IProgressValue<T>, IStartable
    {
        public IProgressable progressable;
        public ILerper<T> lerper;
        public IStartable startable;

        public float Progress => progressable.Progress;
        public bool IsStarted => startable.IsStarted;
        public bool IsFinished => startable.IsFinished;
        public T Value => lerper.Lerp(progressable.Progress);
        
        public LerpCurve(IProgressable progressable, IStartable startable, ILerper<T> lerper)
        {
            this.progressable = progressable;
            this.startable = startable;
            this.lerper = lerper;
        }

        public void Start() => startable.Start();
    }
    //
    // public abstract class JTween<T> : IProgressValue<T>, IStartable, ILerper<T>
    // {
    //     public IProgressable Progressable { get; protected set; }
    //     public IStartable Startable { get; protected set; }
    //     public T StartVal { get; protected set; }
    //     public T EndVal { get; protected set; }
    //
    //     public float Progress => Progressable.Progress;
    //     public T Value => Lerp(Progressable.Progress);
    //     public bool IsStarted => Startable.IsStarted;
    //     public bool IsFinished => Startable.IsStarted;
    //     
    //     public JTween(T startVal, T endVal, IProgressable progressable, IStartable startable)
    //     {
    //         StartVal = startVal;
    //         EndVal = endVal;
    //         Progressable = progressable;
    //         Startable = startable;
    //     }
    //     
    //     public T Lerp(float time) => Lerp(StartVal, EndVal, time);
    //     public void Start() => Startable.Start();
    //     
    //     public abstract T Lerp(T start, T end, float time);
    // }
    //
    // public class TweenV3 : JTween<Vector3>
    // {
    //     public TweenV3(Vector3 startVal, Vector3 endVal, IProgressable progressable, IStartable startable) 
    //         : base(startVal, endVal, progressable, startable) { }
    //     
    //     public override Vector3 Lerp(Vector3 start, Vector3 end, float time)
    //         => Vector3.Lerp(start, end, time);
    // }

    public struct ProgressMap : IProgressable
    {
        [Tooltip("[0,1] -> [0,1]")]
        private readonly AnimationCurve progressCurve;
        public AnimationCurve ProgressCurve => progressCurve;
        public IProgressable progressable { get; private set; }
        public float Progress => ProgressCurve.Evaluate(progressable.Progress);
        
        public ProgressMap(IProgressable progressable, AnimationCurve curve0101)
        {
            this.progressable = progressable;
            this.progressCurve = curve0101;
        }
    }
}