﻿﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace _01_NagaiTools.Editor
{
    [CustomPropertyDrawer(typeof(RangedVectorAttribute))]
    public class RangedVectorDrawer2 : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 2 + 2;
        }

        public override void OnGUI(Rect position, SerializedProperty property,
            GUIContent label)
        {
            position.height = EditorGUIUtility.singleLineHeight;
            var attr = (RangedVectorAttribute) attribute;
            Clamp(position, property.FindPropertyRelative("x"), attr.minX, attr.maxX);
            position.y += EditorGUIUtility.singleLineHeight + 2;
            Clamp(position, property.FindPropertyRelative("y"), attr.minY, attr.maxY);
            property.serializedObject.ApplyModifiedProperties();
        }

        private void Clamp(Rect rect, SerializedProperty prop, float min, float max)
        {
            prop.floatValue = EditorGUI.Slider(rect, prop.displayName, prop.floatValue, min, max);
        }
    }
}