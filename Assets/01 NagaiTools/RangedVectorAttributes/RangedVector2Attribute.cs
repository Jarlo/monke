﻿﻿﻿﻿using UnityEngine;

namespace _01_NagaiTools.Editor
{
    public class RangedVectorAttribute : PropertyAttribute
    {
        public float minX=0;
        public float maxX=1;
        public float minY=0;
        public float maxY=1;

        public Vector2 Clamp(Vector3 v)
        {
            return new Vector2(Mathf.Clamp(v.x, minX, maxX), Mathf.Clamp(v.y, minY, maxY));
        }
    }
}