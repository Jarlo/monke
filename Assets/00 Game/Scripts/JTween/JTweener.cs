﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

namespace _00_Game.Scripts.Flip
{
    public class JTweener
    {
        private float progress;
        private Func<float, float> map = TrivialMap;
        private Action<float> updater;
        private Action onComplete;
        public Tweener CoreTween { get; private set; }

        public float Progress
        {
            get => progress;
            private set
            {
                if (progress == value)
                    return;
                progress = value;
                updater(map(progress));
            }
        }

        public bool IsComplete => progress >= 1;

        public JTweener(Action<float> updater, float duration)
        {
            this.updater = updater;
            CoreTween = Create(duration)
                .OnComplete(OnComplete)
                .OnKill(OnComplete);
        }

        private void OnComplete() => onComplete?.Invoke();

        public JTweener SetOnComplete(Action onComplete)
        {
            this.onComplete = onComplete;
            return this;
        }
        
        public JTweener SetProgressMap(Func<float, float> map)
        {
            this.map = map;
            return this;
        }
        
        private TweenerCore<float, float, FloatOptions> Create(float duration)
        {
            return DG.Tweening.DOTween.To(() => Progress, (p) => Progress = p, 1f, duration).SetEase(Ease.Linear);
        }
        
        private static float TrivialMap(float arg) => arg;
    }
}