﻿using System;
using DG.Tweening;
using UnityEngine;

namespace _00_Game.Scripts.Flip
{
    public static class JTween
    {
        public static JTweener Do<T>(T start, T end, Func<T, T, float, T> lerpFunc, float duration, Action<T> updater)
        {
            return new JTweener((t) => updater(lerpFunc(start, end, Mathf.Clamp01(t))), duration);
        }
        
        public static JTweener Do(Vector3 start, Vector3 end, float duration, Action<Vector3> updater)
        {
            return Do<Vector3>(start, end, Vector3.Lerp, duration, updater);
        }
        
        public static JTweener Do(Quaternion start, Quaternion end, float duration, Action<Quaternion> updater)
        {
            return Do<Quaternion>(start, end, Quaternion.Lerp, duration, updater);
        }
        
        public static JTweener Do(float start, float end, float duration, Action<float> updater)
        {
            return Do<float>(start, end, Mathf.Lerp, duration, updater);
        }

        public static Sequence ToSequence(this Tween tween)
        {
            return DOTween.Sequence().Append(tween);
        }

        public static T AddOnComplete<T>(this T tween, Action callback) where T : Tweener
        {
            var old = tween.onComplete;
            return tween.OnComplete(() =>
            {
                old?.Invoke();
                callback();
            });
        }
    }
}