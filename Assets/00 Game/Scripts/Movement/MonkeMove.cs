﻿using System;
using System.Linq;
using _00_Game.Scripts;
using _00_Game.Scripts.Flip;
using DG.Tweening;
using UnityEngine;

public class MonkeMove : MonoBehaviour
{
    [SerializeField] private BiasedInput input;
    [SerializeField] private Transform controlled;
    [SerializeField] private CollisionManager collMan;
    [Header("Commands")]
    [SerializeField] private JumpTween jumpTween;
    [SerializeField] private SlideTween slideTween;
    [SerializeField] private FallJTween fallTween;
    
    private bool moving;
    public event Action<float> OnFallenEvent;
    public Vector3 Pos => controlled.position;
    
    private void Start()
    {
        input.OnMoveInputEvent += OnMoveInput;
        input.OnJumpInputEvent += OnJumpInput;
    }

    private void OnDestroy()
    {
        input.OnMoveInputEvent -= OnMoveInput;
        input.OnJumpInputEvent -= OnJumpInput;
    }

    private void OnJumpInput()
    {
        if (moving)
            return;

        Tween tween = null;  
        if (TryJump(Vector3.zero, 2, ref tween))
        {
            moving = true;
            tween.OnComplete(CompleteMove);
        }
    }

    private void OnMoveInput(Vector3 direction)
    {
        if (moving)
            return;
        
        Tween tween = null;  
        if (MakeMove(direction, ref tween))
        {
            moving = true;
            tween.OnComplete(CompleteMove);
        }
    }
    
    private bool MakeMove(Vector3 direction, ref Tween result)
    {
        var bad = false;
        if (TrySpecial(direction, ref result, ref bad))
            return true;
        else if (bad)
            return false;
        
        if (TrySlide(direction, ref result))
            return true;
        
        if (TryJump(direction, 1f, ref result))
            return true;
        
        return false;
    }
    
    private void CompleteMove()
    {
        if (TryFall(out var tween))
            tween.OnComplete(CompleteMove);
        else
            moving = false;
    }

    
    private bool TrySlide(Vector3 delta, ref Tween seq)
    {
        if (MaySlide(delta))
            seq = slideTween.DoSlide(controlled, delta);
        else
            return false;
        return true;
    }
    
    private bool MaySlide(Vector3 delta)
    {
        return collMan.IsPassable(Pos + delta);
    }
    
    private bool MayFall()
    {
        return collMan.IsPassable(Pos + Vector3.down);
    }

    private bool TryFall(out Tween tween)
    {
        if(MayFall())
        {
            collMan.TryCastPassable(controlled.position, out var depth);
            tween = fallTween
                .DOFall(controlled, depth)
                .OnComplete(() => OnFallen(depth))
                .ToSequence();
            return true;
        }
        else
        {
            tween = null;
            return false;
        }
    } 
    
    private bool TrySpecial(Vector3 delta, ref Tween seq, ref bool bad)
    {
        var endPos = Pos + delta;
        if (collMan.TryGetComponent<Chest>(endPos, out var chest))
        {
            if (chest.isMoving)
            {
                bad = true;
                return false;
            }
            if(chest.MaySlide(delta))
            {
                chest.Slide(slideTween, fallTween, delta);
                seq = slideTween.DoSlide(controlled, delta);
                return true;
            }
        }
        return false;
    }
    
    private bool TryJump(Vector3 delta, float height, ref Tween seq)
    {
        if (MayJumpOn(delta, height))
            seq = jumpTween.DOJump(controlled, delta, height);
        else
            return false;
        return true;
    }

    private bool MayJumpOn(Vector3 offset, float height)
    {
        var finalOffset = offset + Vector3.up * height;
        return collMan.IsPassable(Pos + Vector3.up) &&
               Enumerable
                   .Range(1, Mathf.CeilToInt(height))
                   .All(i => collMan.IsPassable(Pos + offset + Vector3.up * height));
    }
    
    private void OnFallen(float height)
    {
        OnFallenEvent?.Invoke(height);
    }
}