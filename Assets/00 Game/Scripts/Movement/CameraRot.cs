﻿using System;
using _00_Game.Scripts.Movement;
using UnityEngine;

public class CameraRot : MonoBehaviour
{
    [SerializeField] private BiasedInput input;
    [SerializeField] private Transform camPos;
    [SerializeField] private FloatRange xRange;
    [SerializeField] private FloatRange yRange;
    [SerializeField] private float zeroX;
    [SerializeField] private float zeroY;
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private float xSpeed;
    [SerializeField] private float ySpeed;
    [SerializeField] private float returnCoeff;
    private Vector2 offset;
    
    private KeyCode upCode = KeyCode.I;
    private KeyCode downCode = KeyCode.K;
    private KeyCode leftCode = KeyCode.J;
    private KeyCode rightCode = KeyCode.L;
    private KeyCode stopCode = KeyCode.LeftShift;
    
    
    private void OnCameraInput(Vector3 delta)
    {
        delta.x *= xSpeed * Time.deltaTime;
        delta.y *= ySpeed * Time.deltaTime;
        offset.x = Mathf.Clamp(offset.x + delta.x, -1, 1);
        offset.y = Mathf.Clamp(offset.y + delta.y, -1, 1);
        // Lerp
        camPos.localPosition = new Vector3(
            Lerp(offset.x, xRange, zeroX),
            Lerp(offset.y, yRange, zeroY));
    }

    private float Lerp(float val, FloatRange range, float zero)
    {
        var less = val < zero;
        var time = curve.Evaluate(less ? 1+val : val);
        return less ? 
            Mathf.Lerp(range.min, zero, time) : 
            Mathf.Lerp(zero, range.max, time);
    }

    private void Update()
    {
        UpdateCameraInput();
    }

    private void UpdateCameraInput()
    {
        var dx = Input.GetAxis("CameraX");
        var dy = Input.GetAxis("CameraY");
        var epsilon = 0.01f;

        var stop = !Input.GetKey(stopCode);
        var left = Input.GetKey(leftCode);
        var right = Input.GetKey(rightCode);

        if (left)
            if (!right)
                OnCameraInput(Vector3.left);
            else { } // ok
        else if (right)
            OnCameraInput(Vector3.right);
        else if(stop)
            if (offset.x > epsilon)
                OnCameraInput(Vector3.left * returnCoeff);
            else if (offset.x < -epsilon)
                OnCameraInput(Vector3.right * returnCoeff);


        var up = Input.GetKey(upCode);
        var down = Input.GetKey(downCode);

        if (down)
            if (!up)
                OnCameraInput(Vector3.down);
            else { } // ok
        else if (up)
            OnCameraInput(Vector3.up);
        else if(stop)
            if (offset.y > epsilon)
                OnCameraInput(Vector3.down * returnCoeff);
            else if (offset.y < -epsilon)
                OnCameraInput(Vector3.up * returnCoeff);
    }
}