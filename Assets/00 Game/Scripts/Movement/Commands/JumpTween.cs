﻿using System;
using _00_Game.Scripts.Flip;
using DG.Tweening;
using UnityEngine;

namespace _00_Game.Scripts
{
    public class JumpTween : SimpleJTween
    {
        public Sequence DOJump(Transform moved, Vector3 delta, float height, float duration)
        {
            var offset = delta + Vector3.up * height;
            var target = moved.position + offset;
            return moved
                .DOJump(target, 1f / 2, 1, duration)
                .SetEase(curve)
                .ToSequence();
        }

        public Sequence DOJump(Transform moved, Vector3 delta, float height)
        {
            var realDelta = delta + Vector3.up * height;
            var duration = realDelta.magnitude * config.distUnitDuration * durationCoefficient;
            return DOJump(moved, delta, height, duration);
        }
    }
}