﻿using _00_Game.Scripts.Flip;
using DG.Tweening;
using UnityEngine;

namespace _00_Game.Scripts
{
    public class SlideTween : SimpleJTween
    {
        public Sequence DoSlide(Transform moved, Vector3 delta, float duration)
        {
            var start = moved.position;
            return Flip.JTween
                .Do(start, start + delta, duration, 
                    (v) => moved.position = v)
                .SetProgressMap(curve.Evaluate)
                .CoreTween
                .OnComplete(
                    () => moved.position = start + delta)
                .ToSequence();
        }

        public Sequence DoSlide(Transform controlled, Vector3 delta)
        {
            var duration = delta.magnitude * config.distUnitDuration * durationCoefficient;
            return DoSlide(controlled, delta, duration);
        }
    }
}