﻿using _00_Game.Scripts.Flip;
using DG.Tweening;
using UnityEngine;

namespace _00_Game.Scripts
{
    public class FallJTween : JTween
    {
        [SerializeField] private float gravity = 9.8f;
        
        public Sequence DOFall(Transform moved, float height)
        {
            var start = moved.position;
            var endPos = start + Vector3.down * height;
            var fallTime = Mathf.Sqrt(2 * height / gravity);
            
            void SetPosition(float progress)
            {
                var t = fallTime * progress;
                var delta = gravity * t * t / 2;
                moved.position = start + Vector3.down * delta;
            }
            return Flip.JTween.Do(0, 1, fallTime, SetPosition)
                .CoreTween
                .OnComplete(() => moved.position = endPos)
                .ToSequence();
        }
    }
}