﻿using _00_Game.Scripts.Flip;
using DG.Tweening;
using UnityEngine;

namespace _00_Game.Scripts
{
    public class WorldConfig : ScriptableObject
    {
        public float distUnitDuration = 1f;
    }
}