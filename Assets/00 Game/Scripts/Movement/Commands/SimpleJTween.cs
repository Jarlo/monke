﻿using UnityEngine;

namespace _00_Game.Scripts
{
    public class SimpleJTween : JTween
    {
        [SerializeField] protected AnimationCurve curve;
        [SerializeField] protected float durationCoefficient = 1f;
    }
}