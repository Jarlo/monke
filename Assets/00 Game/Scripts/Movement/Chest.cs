﻿using _00_Game.Scripts;
using DG.Tweening;
using UnityEngine;

public class Chest : MonoBehaviour
{
    [SerializeField] private CollisionManager collMan;
    public Vector3 Pos => transform.position;
    public bool isMoving;

    private bool MayFall()
    {
        return collMan.IsPassable(Pos);
    }

    public bool MaySlide(Vector3 delta)
    {
        return (collMan.IsPassable(Pos + delta));
    }

    public void Slide(SlideTween slideTween, FallJTween fallTween, Vector3 delta)
    {
        isMoving = true;
        slideTween.DoSlide(transform, delta)
            .OnComplete(OnComplete);
        
        void OnComplete()
        {
            if (collMan.IsPassable(Pos + Vector3.down))
            {
                collMan.TryCastPassable(Pos, out var depth);
                fallTween.DOFall(transform, depth);
            }
            else
                isMoving = false;
        }
    }
}