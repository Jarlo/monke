﻿using System;
using _00_Game.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthManager : MonoBehaviour
{
    [SerializeField] private MonkeMove monkeMove;
    [SerializeField] private float deathHeight = 3;

    private void Start()
    {
        monkeMove.OnFallenEvent += OnFallen;
    }

    private void OnDestroy()
    {
        monkeMove.OnFallenEvent -= OnFallen;
    }

    private void OnFallen(float height)
    {
        if(height >= deathHeight)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}