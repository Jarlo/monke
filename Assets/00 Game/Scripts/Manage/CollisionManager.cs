﻿using System.Linq;
using UnityEngine;

public class CollisionManager : MonoBehaviour
{
    [SerializeField] private LayerMask impassable;

    public bool TryGetComponent<T>(Vector3 position, out T component) where T: MonoBehaviour
    {
        var colliders = Physics.OverlapBox(position, Vector3.one / 4, Quaternion.identity);
        foreach (var collider in colliders)
            if (collider.TryGetComponent<T>(out var comp))
            {
                component = comp;
                return true;
            }
        component = default;
        return false;
    }
    
    public bool IsPassable(Vector3 position)
    {
        return !HasLayer(position, impassable);
    }
    
    public bool HasLayer(Vector3 position, int mask)
    {
        return Physics.CheckBox(position, Vector3.one / 4, Quaternion.identity, impassable);
    }

    public bool TryCastPassable(Vector3 position, out float depth, float maxDepth = 10)
    {
        var offset = position;
        for (int i = 0; i < maxDepth; i++)
        {
            offset += Vector3.down;
            if (!IsPassable(offset))
            {
                depth = i;
                return true;
            }
        }
        depth = maxDepth;
        return false;
    }
}