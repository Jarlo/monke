﻿using System;
using UnityEngine;

public class BiasedInput : MonoBehaviour
{
    [SerializeField] private Transform viewCenter;
    public event Action<Vector3> OnMoveInputEvent;
    public event Action OnJumpInputEvent;
    
    private void Update()
    {
        UpdateMoveInput();
    }

    private void UpdateMoveInput()
    {
        var dz = Input.GetAxis("Vertical");
        var dx = Input.GetAxis("Horizontal");
        
        if(Input.GetKey(KeyCode.Space))
            OnJumpInputEvent?.Invoke();
        
        if (dx > 0)
            OnMoveInput(MapDirection(Vector3.right));
        else if (dx < 0)
            OnMoveInput(MapDirection(Vector3.left));
        
        if (dz > 0)
            OnMoveInput(MapDirection(Vector3.forward));
        else if (dz < 0)
            OnMoveInput(MapDirection(Vector3.back));
    }
    

    private void OnMoveInput(Vector3 direction)
    {
        OnMoveInputEvent?.Invoke(direction);
    }

    private Vector3 MapDirection(Vector3 direction)
    {
        var dirs = new []{viewCenter.forward, -viewCenter.forward, viewCenter.right, -viewCenter.right};
        var closest = Vector3.zero;
        var minDist = float.PositiveInfinity;
        foreach (var dir in dirs)
        {
            var dist = Vector3.Distance(direction, dir);
            if (minDist > dist)
            {
                closest = dir;
                minDist = dist;
            }
        }
        return closest;
    }
}